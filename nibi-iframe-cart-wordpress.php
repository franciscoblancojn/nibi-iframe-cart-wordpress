<?php
/*
Plugin Name: Nibi Iframe Cart Wordpress
Plugin URI: https://gitlab.com/franciscoblancojn/nibi-iframe-cart-wordpress
Description: Plugin for Wordpress, para agregar iframe Nibi en Carrito, use [NICW_showIframe]
Author: Francisco Blanco
Version: 1.0.5
Author URI: https://franciscoblanco.vercel.app/
*/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/franciscoblancojn/nibi-iframe-cart-wordpress',
	__FILE__,
	'nibi-iframe-cart-wordpress'
);
$myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');
$myUpdateChecker->setBranch('master');

function NICW_get_version() {
    $plugin_data = get_plugin_data( __FILE__ );
    $plugin_version = $plugin_data['Version'];
    return $plugin_version;
}

define("NICW_LOG",true);
define("NICW_PATH",plugin_dir_path(__FILE__));
define("NICW_URL",plugin_dir_url(__FILE__));

require_once NICW_PATH . "src/_index.php";