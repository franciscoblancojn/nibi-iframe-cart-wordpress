<?php

function NICW_showIframe()
{
    $cart = NICW_getCart();

    $cartString = json_encode($cart);

    $cartBase64 = base64_encode($cartString);

    ob_start();
    ?>
        <link rel="stylesheet" href="<?=NICW_URL?>src/css/iframe.css">
        <iframe id="iframeNICW" src="https://nibi.com.co/ext/pagos/mqtl/<?=$cartBase64?>" style="border: none;" width="100%" height="600px">
    <?php
    return ob_get_clean();
}
add_shortcode( 'NICW_showIframe', 'NICW_showIframe' );
 
add_action( 'woocommerce_after_cart', 'NICW_showIframe', 20 );