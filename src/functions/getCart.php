<?php

function NICW_getCart()
{
    global $woocommerce;
    $items = $woocommerce->cart->get_cart();

    $products = [];

    foreach($items as $item => $values) { 
        $product =  wc_get_product( $values['product_id'] );

        $products[] = array(
            "id"            => $values['product_id'],
            "amount"        => $values['quantity'],
            "price"         => $product->get_price(),
            "name"          => $product->get_title(),
        );
    }
    return array(
        "products" => $products,
    );
}